﻿
''' <summary>
''' Deterministicky prohazi poradi / promicha zadanou posloupnost.
''' </summary>
''' <typeparam name="T">Typ itemu posloupnosti</typeparam>
Public Class Scrambler(Of T)

  Public Const GoldenRatio As Double = 1.61803398875

  ''' <summary>
  ''' Pokud kod pro generovani ruznych patternu tolikrat za sebou vygeneruje neco, co uz existuje, tak to zabali a dal uz se nesnazi generovat.
  ''' </summary>
  Private Const LoopThreshold As Integer = 100

  ''' <summary>
  ''' S jakym cislem zacit pri generovani posloupnosti
  ''' </summary>
  ''' <returns></returns>
  Public ReadOnly Property Seed As Double

  Public Sub New()
    Me.New(0.0)
  End Sub

  Public Sub New(seed As Double)
    Me.Seed = seed
  End Sub

  ''' <summary>
  ''' Deterministicky rozhazi itemy
  ''' </summary>
  ''' <param name="items">Itemy k rozhazeni</param>
  Public Function Scramble(ParamArray items As T()) As T()
    Return MultiScramble(1, items)(0)
  End Function

  ''' <summary>
  ''' Deterministicky rozhazi itemy
  ''' </summary>
  ''' <param name="n">Pocet prohazenych vysledku</param>
  ''' <param name="items">Itemy k rozhazeni</param>
  Public Function MultiScramble(n As Integer, items As IEnumerable(Of T)) As List(Of T())
    Dim itemsArr = items.ToArray()
    Dim result As New List(Of T())() From {}
    Dim patterns = Me.GenDistinctPatterns(itemsArr.Length, n)
    For Each pattern In patterns
      result.Add((From i In pattern Select itemsArr(i)).ToArray())
    Next
    Return result
  End Function

  ''' Vrati <param name="m" /> rozdilnych patternu. Muze se stat, ze jich vrati mene, pokud se je nepodari vygenerovat.
  ''' <param name="n">Pocet cisel v patternu</param>
  ''' <param name="m">Pocet patternu</param>
  Private Function GenDistinctPatterns(n As Integer, ByVal m As Integer) As List(Of Integer())
    Dim result As New List(Of Integer())()
    Dim seed = Me.Seed
    Dim check = 0
    While result.Count < m And check < LoopThreshold
      Dim current = GenPattern(n, seed)
      Dim patternUzObsazenVResultu = result.Any(Function(item) EnumerableHelper.JsouPosloupnostiStejne(current, item))
      If patternUzObsazenVResultu Then
        check += 1
      Else
        check = 0
        result.Add(current)
      End If
    End While
    Return result
  End Function

  ''' <summary>
  ''' Vygeneruje co nejvice neusporadane poradi. vysledek jsou cisla od 0 do <paramref name="n"/>.
  ''' </summary>
  ''' <param name="n">Pocet cisel v patternu</param>
  ''' <param name="seed">Pocatecni cislo od kterho se odviji vygenerovane hodnoty. Slouzy zaroven jako vystupni hodnota, na ktere generator skoncil.</param>
  Private Function GenPattern(n As Integer, ByRef seed As Double) As Integer()
    Dim posun = n / GoldenRatio
    Dim cisla = New HashSet(Of Integer)(Enumerable.Range(0, n))
    Dim result As New List(Of Integer)()
    While result.Count < n
      Dim i = CInt(Math.Round(seed)) Mod n
      While Not cisla.Contains(i)
        i += 1
        i = i Mod n
      End While
      cisla.Remove(i)
      result.Add(i)
      seed += posun
    End While
    Return result.ToArray()
  End Function

End Class

Module ScramblerTest

  Public Sub Test()
    Dim scrambler As New Scrambler(Of Integer)()
    Dim vygenerovaneMichani = scrambler.MultiScramble(1000, Enumerable.Range(1, 3))
    For i = 0 To vygenerovaneMichani.Count - 1
      Dim a = vygenerovaneMichani(i)
      Dim opakujeSe = vygenerovaneMichani.Take(i).Any(Function(b) EnumerableHelper.JsouPosloupnostiStejne(a, b))
      Console.Write($"{opakujeSe}, {i + 1}: ")
      Console.WriteLine(String.Join(", ", a))

    Next
    'For n = 3 To 16
    '  Dim pattern = Scrambler.GenPattern(n)
    '  Print(pattern)
    'Next
  End Sub

  Private Sub Print(header As String, a As IEnumerable(Of Integer))
    Console.WriteLine(header + String.Join(", ", a))
  End Sub

End Module

