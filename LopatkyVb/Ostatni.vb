﻿Class Cache(Of TKey, TValue)

  Private ReadOnly Items As New System.Collections.Concurrent.ConcurrentDictionary(Of TKey, TValue)()

  Private ReadOnly GenerateValue As Func(Of TKey, TValue)

  Public Sub New(generateValue As Func(Of TKey, TValue))
    Me.GenerateValue = generateValue
  End Sub

  Public Function GetValue(key As TKey) As TValue
    Dim result As TValue
    If Not Me.Items.TryGetValue(key, result) Then
      result = GenerateValue(key)
      Me.Items.TryAdd(key, result)
    End If
    Return result
  End Function

End Class

Module EnumerableHelper

  ''' <summary>
  ''' Vrati informaci o tom, jestli posloupnost <paramref name="a"/> obsahuje stejne prvky, ve stejnem poradi a stejny pocet jako posloupnost <paramref name="b"/>.
  ''' </summary>
  Public Function JsouPosloupnostiStejne(Of T)(a As T(), b As T()) As Boolean
    Return JsouPosloupnostiStejne(Of T)(a, b, EqualityComparer(Of T).Default)
  End Function

  ''' <summary>
  ''' Vrati informaci o tom, jestli posloupnost <paramref name="a"/> obsahuje stejne prvky, ve stejnem poradi a stejny pocet jako posloupnost <paramref name="b"/>.
  ''' </summary>
  ''' <param name="comparer">Co pouzit k porovnani</param>
  Public Function JsouPosloupnostiStejne(Of T)(a As T(), b As T(), comparer As IEqualityComparer(Of T)) As Boolean
    Return a.Length = b.Length AndAlso a.Zip(b, AddressOf comparer.Equals).All(Function(i) i)
  End Function

End Module