Imports System
Imports System.Collections.Concurrent
Imports LopatkyVb

Module Program

  ''' <summary>
  ''' Ruzny lopatky pro testovani
  ''' </summary>
  ReadOnly zadaniList As New List(Of Double()) From {
      New Double() {500.0, 480.0, 490.0, 475.0, 492.0, 510.0, 505.0, 520.0, 470.0},
      New Double() {10300.0, 10300.0, 10210.0, 10160.0, 10130.0, 10130.0, 10120.0, 10120.0, 10100.0, 10100.0, 10090.0, 10090.0, 10080.0, 10070.0, 10050.0, 10050.0, 10030.0, 10020.0},
      GenerovatNahodneHodnoty(91, 10000.0, 10200.0)
    }

  Sub Main(args As String())

    'PorovnatOptimalizatory()
    'MichaciOptimalizer()

    ' Musis nejak sestavit pole nebo list integeru, ktere nejakyum zpusobem prectese z ListBoxu
    Dim hodnotyZListBoxu = New Integer() {500, 480, 490, 475, 492, 510, 505, 520, 470}

    ' Zoptimalizovat. Vysledek je zase pole integeru.
    Dim zoptimalizovano = Seradit(hodnotyZListBoxu)

    ' Pak to dale zpracovat. Treba vypsat nebo cokoliv
    Console.WriteLine(String.Join(", ", zoptimalizovano))

    Console.WriteLine()
    Console.WriteLine("Neco zmacknout pro ukonceni")
    Console.ReadKey()
  End Sub

  ''' <summary>
  ''' Vrati pole serazenych hodnot.
  ''' </summary>
  ''' <param name="hmotnosti">hodnoty k serazeni</param>
  Function Seradit(hmotnosti As IEnumerable(Of Integer)) As Integer()
    ' Inicializovat optimalizer, ktery provadi samotny algoritmus
    Dim rozvazny = New RozvaznyOptimalizer("Rozvazny")

    ' Zabalit ho do wrapperu, ktery bude zkouset ruzne promichany vstupy
    Dim kolikratPromichat = 30
    Dim rozvaznyScrambled = New ScrambleOptimalizer("Zamichany rozvazny", kolikratPromichat, rozvazny)

    ' Vytvorit kolo (ve vnitr pracuju s Doublama, vstup jsoi inty takze to potrebuju konvertovat)
    Dim pocatecniKolo = New Kolo((From h In hmotnosti Select CDbl(h)).ToArray())

    ' Optimalizovat
    Dim optimalizovaneKolo = rozvaznyScrambled.Optimalizovat(pocatecniKolo)

    ' Vratit vysledne poradi (tady to zase konvertuju zpet z doublu do intu)
    Return (From h In optimalizovaneKolo.SerazeneHmotnosti Select CInt(h)).ToArray()
  End Function

  Sub MichaciOptimalizer()
    ' Inicializovat optimalizer, ktery provadi samotny algoritmus
    Dim rozvazny = New RozvaznyOptimalizer("Rozvazny")

    ' Zabalit ho do wrapperu, ktery bude zkouset ruzne promichany vstupy
    Dim kolikratPromichat = 30
    Dim rozvaznyScrambled = New ScrambleOptimalizer("Zamichany rozvazny", kolikratPromichat, rozvazny)

    ' Otestovat na ruznych kolech
    For Each zadani In zadaniList
      Dim pocatecniKolo = New Kolo(zadani)
      Console.WriteLine("Vstupni kolo:")
      VypsatKolo(pocatecniKolo)
      Console.WriteLine()
      OptimalizovatAVypsatVysledekPorovnaniKola(pocatecniKolo, rozvaznyScrambled)
    Next

  End Sub

  Private Sub PorovnatOptimalizatory()
    For Each zadani In zadaniList
      PorovnatOptimalizatory(zadani)
    Next
  End Sub

  Private Sub PorovnatOptimalizatory(ParamArray hmotnosti As Double())

    Dim pocatecniKolo = New Kolo(hmotnosti)
    Console.WriteLine("Vstupni kolo:")
    VypsatKolo(pocatecniKolo)
    Console.WriteLine()

    Dim rozvazny = New RozvaznyOptimalizer("Rozvazny")
    Dim strelec = New StrelecOptimalizer("Strelec")
    Dim optimalizery = New IOptimalizer() {
      rozvazny,
      strelec,
      New MultiOptimalizer("Rozvazny strelec", rozvazny, strelec),
      New ScrambleOptimalizer("Zamichany rozvazny", 30, rozvazny)
    }
    For Each optimalizer In optimalizery
      OptimalizovatAVypsatVysledekPorovnaniKola(pocatecniKolo, optimalizer)
    Next
    Console.WriteLine("******")
    Console.WriteLine()
  End Sub

  Private Sub VypsatKolo(kolo As Kolo)
    Console.WriteLine("Vzdalenost teziste od pocatku: {0:N9}", Math.Sqrt(kolo.Teziste.VzdalenostOdPocatkuKvadrat))
    Console.WriteLine(String.Join("; ", From l In kolo Select String.Format("{0:N1}", l.Hmotnost)))
  End Sub

  Private Sub OptimalizovatAVypsatVysledekPorovnaniKola(pocatecniKolo As Kolo, optimalizer As IOptimalizer)
    Dim w As New Stopwatch()
    w.Start()
    Dim optimalizovaneKolo = optimalizer.Optimalizovat(pocatecniKolo)
    w.Stop()

    Console.WriteLine("Vysledek podle optimalizeru {0}:", optimalizer.Name)
    If pocatecniKolo.MaStejneLopatky(optimalizovaneKolo) Then
      VypsatKolo(optimalizovaneKolo)
    Else
      Console.WriteLine("!!! Pozor: Neco je spatne! Tohle kolo neni stastaveno ze stejnych lopatek jako zadani.")
    End If
    Console.WriteLine("Optimalizace travala {0:N0} ms", w.ElapsedMilliseconds)
    Console.WriteLine()
  End Sub

  Private Function GenerovatNahodneHodnoty(pocet As Integer, min As Double, max As Double) As Double()
    Dim rng = New Random()
    Dim r(pocet - 1) As Double
    Dim range = max - min
    For i = 0 To r.Length - 1
      r(i) = rng.NextDouble() * range + min
    Next
    Return r
  End Function

End Module

