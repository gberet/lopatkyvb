﻿Imports LopatkyVb

Interface IOptimalizer

  ReadOnly Property Name As String

  Function Optimalizovat(pocatecniKolo As Kolo) As Kolo

End Interface

''' <summary>
''' Funguje nasledujicim zpusobem: 
''' 1. Vezme vstupní hodnoty a "vyrobí" z nich oběžné kolo. Následně zkouší prohodit vždycky pár lopatek a zjistuje, která verze je nejlepší (= má těžiště nejblíže středu).
''' 2. Vezme nejlepší verzi a opakuje od bodu 1 dokud došlo k nějakýmu prohození (v určitým momentu každé prohození vede ke zhoršení).
''' </summary>
Class RozvaznyOptimalizer
  Implements IOptimalizer

  Private Const MaxPocetIteraci As Integer = 10000

  Public Sub New(name As String)
    Me.Name = name
  End Sub

  Public ReadOnly Property Name As String Implements IOptimalizer.Name

  Public Function Optimalizovat(pocatecniKolo As Kolo) As Kolo Implements IOptimalizer.Optimalizovat
    Dim iterace = 1
    Dim kolo = pocatecniKolo
    While iterace < MaxPocetIteraci AndAlso Iterovat(kolo)
      iterace += 1
    End While
    Return kolo
  End Function

  ''' <summary>
  ''' Vrati true, pokud doslo k prohozeni
  ''' </summary>
  ''' <param name="kolo">Pouzije se zaroven jako vstupni hodnota pro kolo, ktere se ma optimalizovat, tak jako vystupni hodnota pro vysledne kolo.</param>
  Function Iterovat(ByRef kolo As Kolo) As Boolean
    Dim pocatecni = kolo
    Dim dosloKProhozeni = False
    For a = 0 To pocatecni.Count - 1
      For b = a + 1 To pocatecni.Count - 1
        Dim navrh = pocatecni.ProhoditLopatky(a, b)
        If navrh.Teziste.VzdalenostOdPocatkuKvadrat < kolo.Teziste.VzdalenostOdPocatkuKvadrat Then
          kolo = navrh
          dosloKProhozeni = True
        End If
      Next
    Next
    Return dosloKProhozeni
  End Function

End Class

''' <summary>
''' Funguje nasledujicim zpusobem: 
''' 1. Vezme vstupní hodnoty a "vyrobí" z nich oběžné kolo. Následně zkouší prohodit vždycky pár lopatek. 
''' 2. Jakmile prohozeni vede ke zlepseni (=teziste blize stredu), opakuje iteraci od bodu 1 dokud došlo k nějakýmu prohození (v určitým momentu každé prohození vede ke zhoršení).
''' </summary>
Class StrelecOptimalizer
  Implements IOptimalizer

  Private Const MaxPocetIteraci As Integer = 10000

  Public Sub New(name As String)
    Me.Name = name
  End Sub

  Public ReadOnly Property Name As String Implements IOptimalizer.Name

  Public Function Optimalizovat(pocatecniKolo As Kolo) As Kolo Implements IOptimalizer.Optimalizovat
    Dim iterace = 1
    Dim kolo = pocatecniKolo
    While iterace < MaxPocetIteraci AndAlso Iterovat(kolo)
      iterace += 1
    End While
    Return kolo
  End Function

  ''' <summary>
  ''' Vrati true, pokud doslo k prohozeni
  ''' </summary>
  ''' <param name="kolo">Pouzije se zaroven jako vstupni hodnota pro kolo, ktere se ma optimalizovat, tak jako vystupni hodnota pro vysledne kolo.</param>
  Function Iterovat(ByRef kolo As Kolo) As Boolean
    Dim pocatecni = kolo
    For a = 0 To pocatecni.Count - 1
      For b = a + 1 To pocatecni.Count - 1
        Dim navrh = pocatecni.ProhoditLopatky(a, b)
        If navrh.Teziste.VzdalenostOdPocatkuKvadrat < kolo.Teziste.VzdalenostOdPocatkuKvadrat Then
          kolo = navrh
          Return True
        End If
      Next
    Next
    Return False
  End Function

End Class

''' <summary>
''' Kombinuje ruzne optimalizery - z jejich vysldku vybere ten lepsi.
''' </summary>
Class MultiOptimalizer
  Implements IOptimalizer

  Private ReadOnly Optimalizery As IReadOnlyList(Of IOptimalizer)

  Public Sub New(name As String, ParamArray optimalizery As IOptimalizer())
    Me.Optimalizery = New List(Of IOptimalizer)(optimalizery)
    Me.Name = name
  End Sub

  Public ReadOnly Property Name As String Implements IOptimalizer.Name

  Public Function Optimalizovat(pocatecniKolo As Kolo) As Kolo Implements IOptimalizer.Optimalizovat
    Dim vysledek As Kolo = Me.Optimalizery(0).Optimalizovat(pocatecniKolo)
    For index = 1 To Me.Optimalizery.Count - 1
      Dim a = Optimalizery(index).Optimalizovat(pocatecniKolo)
      If a.Teziste.VzdalenostOdPocatkuKvadrat < vysledek.Teziste.VzdalenostOdPocatkuKvadrat Then
        vysledek = a
      End If
    Next
    Return vysledek
  End Function

End Class

''' <summary>
''' Wrapper pro optimalizer. Zkusi optimalizer krmit ruzne rozhazenyma vstupama a vybere ten nejlepsi vysledek.
''' </summary>
Class ScrambleOptimalizer
  Implements IOptimalizer

  Public ReadOnly Property Name As String Implements IOptimalizer.Name

  ''' <summary>
  ''' Kolik ruzne rozhazenych variant kola vyzkousek.
  ''' </summary>
  Public ReadOnly Property PocetVariant As Integer

  Private ReadOnly Optimalizer As IOptimalizer

  Public Sub New(name As String, pocetVariant As Integer, optimalizer As IOptimalizer)
    If optimalizer Is Nothing Then Throw New ArgumentNullException(NameOf(optimalizer))
    Me.Name = name
    Me.PocetVariant = pocetVariant
    Me.Optimalizer = optimalizer
  End Sub

  Public Function Optimalizovat(result As Kolo) As Kolo Implements IOptimalizer.Optimalizovat
    Dim rozhazenaKola = Prohazet(result)
    For Each navrh In rozhazenaKola
      navrh = Me.Optimalizer.Optimalizovat(navrh)
      If navrh.Teziste.VzdalenostOdPocatkuKvadrat < result.Teziste.VzdalenostOdPocatkuKvadrat Then
        result = navrh
      End If
    Next
    Return result
  End Function

  ''' <summary>
  ''' Vygeneruje ruzne varianty obezneho kola k vyzkouseni. Vystup obsahuje i puvodni zadani.
  ''' </summary>
  Private Function Prohazet(kolo As Kolo) As List(Of Kolo)
    Dim result As New List(Of Kolo) From {kolo}
    Dim scrambler = New Scrambler(Of Double)()
    Dim scrambled = scrambler.MultiScramble(Me.PocetVariant - 1, From lopatka In kolo Select lopatka.Hmotnost)
    result.AddRange(From h In scrambled Select New Kolo(h))
    Return result
  End Function

End Class