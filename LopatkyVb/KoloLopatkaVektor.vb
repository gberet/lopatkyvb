﻿Class Kolo
  Implements IReadOnlyList(Of Lopatka)

  Private ReadOnly Lopatky As IReadOnlyList(Of Lopatka)

  ''' <summary>
  ''' Nenulove cislo
  ''' </summary>
  ''' <returns></returns>
  Public ReadOnly Property Hmotnost As Double

  Private ReadOnly _Teziste As Lazy(Of Vektor)

  Public ReadOnly Property Teziste As Vektor
    Get
      Return Me._Teziste.Value
    End Get
  End Property

  ''' <summary>
  ''' Hmotnosti lopatek tak, jak jsou postupne rozmisteny na kole.
  ''' </summary>
  ''' <returns></returns>
  Public ReadOnly Property SerazeneHmotnosti As Double()
    Get
      Return (From l In Me.Lopatky Select l.Hmotnost).ToArray()
    End Get
  End Property

  Public Sub New(ParamArray hmotnosti As Double())
    If hmotnosti Is Nothing Then Throw New ArgumentNullException(NameOf(hmotnosti))
    Me.Hmotnost = hmotnosti.Sum()
    If Me.Hmotnost = 0.0 Then Throw New ArgumentException("Soucet hmotnosti nesmi byt nula.", NameOf(hmotnosti))
    Dim polohy = Kolo.PolohyLopatek.GetValue(hmotnosti.Length)
    Me.Lopatky = New List(Of Lopatka)(hmotnosti.Zip(polohy, AddressOf Lopatka.Create))
    Me._Teziste = New Lazy(Of Vektor)(AddressOf Me.SpocitatTeziste)
  End Sub

  Private Function SpocitatTeziste() As Vektor
    Dim result = Vektor.Origin
    For Each lopatka In Me.Lopatky
      result += lopatka.Hmotnost * lopatka.Teziste
    Next
    Return result / Me.Hmotnost
  End Function

  ''' <summary>
  ''' Pro kazdy pocet lopatek vrati rozlozeni uhlu
  ''' </summary>
  Private Shared ReadOnly Property PolohyLopatek As New Cache(Of Integer, IReadOnlyList(Of Double))(AddressOf GetPolohy)

  Private Shared Function GetPolohy(ByVal n As Integer) As IReadOnlyList(Of Double)
    Return New List(Of Double)(From i In Enumerable.Range(1, n) Select 2.0 * Math.PI * i / n)
  End Function

  ''' <summary>
  ''' Vytvori nove kolo s prohozenyma lopatkama na pozicich <paramref name="a"/> a <paramref name="b"/> (pocitano on nuly).
  ''' </summary>
  ''' <param name="a">index lopatky</param>
  ''' <param name="b">index druhe lopatky</param>
  Public Function ProhoditLopatky(a As Integer, b As Integer) As Kolo
    Dim h(Me.Lopatky.Count - 1) As Double
    For i = 0 To h.Length - 1
      Dim j = i
      If i = a Then
        j = b
      ElseIf i = b Then
        j = a
      End If
      h(i) = Me.Lopatky(j).Hmotnost
    Next
    Return New Kolo(h)
  End Function

  ''' <summary>
  ''' Zjisti, jestli je toto kolo sestavene ze stejnych lopatek jako <paramref name="jineKolo"/>.
  ''' </summary>
  Public Function MaStejneLopatky(jineKolo As Kolo) As Boolean
    Dim a = New HashSet(Of Double)(Me.SerazeneHmotnosti)
    Return a.SetEquals(jineKolo.SerazeneHmotnosti)
  End Function

#Region "Implemetace IReadOnlyList"

  Default Public ReadOnly Property Item(index As Integer) As Lopatka Implements IReadOnlyList(Of Lopatka).Item
    Get
      Return Lopatky(index)
    End Get
  End Property

  Public ReadOnly Property Count As Integer Implements IReadOnlyCollection(Of Lopatka).Count
    Get
      Return Lopatky.Count
    End Get
  End Property

  Public Function GetEnumerator() As IEnumerator(Of Lopatka) Implements IEnumerable(Of Lopatka).GetEnumerator
    Return Lopatky.GetEnumerator()
  End Function

  Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
    Return Lopatky.GetEnumerator()
  End Function

#End Region

End Class

Class Lopatka

  Private Sub New(hmotnost As Double, poloha As Double, teziste As Vektor)
    If teziste Is Nothing Then
      Throw New ArgumentNullException(NameOf(teziste))
    End If

    Me.Hmotnost = hmotnost
    Me.Poloha = poloha
    Me.Teziste = teziste
  End Sub

  Public ReadOnly Property Hmotnost As Double

  ''' <summary>
  ''' Uhel v radianech
  ''' </summary>
  Public ReadOnly Property Poloha As Double

  Public ReadOnly Property Teziste As Vektor

  Public Shared Function Create(hmotnost As Double, poloha As Double) As Lopatka
    Return New Lopatka(hmotnost, poloha, Lopatka.TezisteLopatek.GetValue(poloha))
  End Function


  ''' <summary>
  ''' Vrati teziste pro polohy / uhly lopatek.
  ''' </summary>
  Private Shared ReadOnly Property TezisteLopatek As New Cache(Of Double, Vektor)(AddressOf TezisteLopatky)

  Private Shared Function TezisteLopatky(ByVal uhel As Double) As Vektor
    Return New Vektor(Math.Cos(uhel), Math.Sin(uhel))
  End Function

End Class

Class Vektor

  Public Shared ReadOnly Property Origin As New Vektor(0.0, 0.0)

  Public ReadOnly Property X As Double

  Public ReadOnly Property Y As Double

  Private ReadOnly _VzdalenostOdPocatkuKvadrat As Lazy(Of Double)

  Public ReadOnly Property VzdalenostOdPocatkuKvadrat As Double
    Get
      Return Me._VzdalenostOdPocatkuKvadrat.Value
    End Get
  End Property

  Public Sub New(x As Double, y As Double)
    Me.X = x
    Me.Y = y
    Me._VzdalenostOdPocatkuKvadrat = New Lazy(Of Double)(AddressOf SpocitatVzdalenostOdPocatkuKvadrat)
  End Sub

  Private Function SpocitatVzdalenostOdPocatkuKvadrat() As Double
    Return Me.X * Me.X + Me.Y * Me.Y
  End Function

  Public Overrides Function ToString() As String
    Return $"[{X:N3}, {Y:N3}]"
  End Function

  Public Shared Operator *(left As Double, right As Vektor) As Vektor
    Return New Vektor(left * right.X, left * right.Y)
  End Operator

  Public Shared Operator +(left As Vektor, right As Vektor) As Vektor
    Return New Vektor(left.X + right.X, left.Y + right.Y)
  End Operator

  Public Shared Operator /(left As Vektor, right As Double) As Vektor
    Return New Vektor(left.X / right, left.Y / right)
  End Operator

End Class